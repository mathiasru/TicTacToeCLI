package com.company;

public class Spielfeld {
    //Datenfeld
    private final String[][] feld;
    private Spieler spieler;

    public Spielfeld(int reihe, int splate) {
        feld = new String[reihe][splate];
        for (int i = 0; i < feld.length; i++) {
            for (int j = 0; j < feld[i].length; j++) {
                feld[i][j] = " ";
            }
        }
    }

    public boolean setzeSpielstein(int reihe, int spalte, Spieler aktuellerSpieler) {
        if (feld[reihe][spalte].equals(" ")) {
            feld[reihe][spalte] = aktuellerSpieler.getSpielerZeichen();
            return true;
        } else {
            System.out.println("Stein konnte nicht gesetzt werden, Feld schon belegt!");
            return false;
        }
    }

    public void spielfeldAusgeben(int reihe, int spalte) {
        for (int i = 0; i < feld.length; i++) {
            for (int j = 0; j < feld[i].length; j++) {
                System.out.print("| " + feld[i][j] + " ");
                if (j == feld[i].length - 1) {
                    System.out.print("|");
                }
            }
            System.out.println();
        }
    }

    public boolean ueberpruefeSpielfeld(String spielerZeichen) {
        return ueberpruefeHorizontal(spielerZeichen) || ueberpruefeVertikal(spielerZeichen) || ueberpruefeQuer(spielerZeichen);
    }

    public boolean ueberpruefeHorizontal(String spielerZeichen) {
        boolean ueberpruefung = false;
        for (int i = 0; i < feld.length; i++) {
            if (feld[i][0].equals(spielerZeichen) && (feld[i][1].equals(spielerZeichen) && (feld[i][2].equals(spielerZeichen)))) {
                ueberpruefung = true;
            }
        }
        return ueberpruefung;
    }

    public boolean ueberpruefeVertikal(String spielerZeichen) {
        boolean ueberpruefung = false;
        for (int i = 0; i < feld[0].length; i++) {
            if (feld[0][i].equals(spielerZeichen) && (feld[1][i].equals(spielerZeichen) && (feld[2][i].equals(spielerZeichen)))) {
                ueberpruefung = true;
            }
        }
        return ueberpruefung;
    }

    public boolean ueberpruefeQuer(String spielerZeichen) {
        boolean ueberpruefung = false;
        if (feld[0][0].equals(spielerZeichen) && feld[1][1].equals(spielerZeichen) && feld[2][2].equals(spielerZeichen)) {
            ueberpruefung = true;
        } else if (feld[0][2].equals(spielerZeichen) && feld[1][1].equals(spielerZeichen) && feld[2][0].equals(spielerZeichen)) {
            ueberpruefung = true;
        }
        return ueberpruefung;
    }

    public boolean spielUnentschieden() {
        int anzahlGelegtSteine = 0;
        for (int i = 0; i < feld.length; i++) {
            for (int j = 0; j < feld[i].length; j++) {
                if (!feld[i][j].equals(" ")) {
                    anzahlGelegtSteine++;
                }
            }
        }
        return anzahlGelegtSteine == (feld.length * feld[0].length);
    }
}
